package core.exceptions

data class UserNotFound(val code: String, override val message: String) : Throwable()