package core.exceptions

data class UserAlreadyTakenError(val code: String, override val message: String) : Throwable()