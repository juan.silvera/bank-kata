package core.actions

import core.domain.User
import core.exceptions.UserAlreadyTakenError
import core.repository.UserRepository

class RegisterUser(private val repository: UserRepository) {

    operator fun invoke(name: String, nickname: String): Boolean {
        val userFound = repository.get(nickname)

        if (userFound == null) {
            repository.save(User(name, nickname))

            return true
        } else {
            throw UserAlreadyTakenError("666", "Nickname duplicado")
        }
    }
}