package core.actions

import core.domain.User
import core.exceptions.UserNotFound
import core.repository.UserRepository

class UpdateUser(private val repository: UserRepository) {
    fun invoke(newName: String, nickname: String): Boolean {
        val userToUpdate = repository.get(nickname)

        return if (userToUpdate != null) {
            repository.save(User(newName, nickname))

            true
        } else {
            throw UserNotFound("555", "No se encontró el usuario")
        }
    }
}
