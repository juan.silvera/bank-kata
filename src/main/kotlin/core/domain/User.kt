package core.domain

data class User(private var name: String, val nickname: String)
