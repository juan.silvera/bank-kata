package core.repository

import core.domain.User

interface UserRepository {
    fun save(user: User)
    fun get(nickname: String): User?
}
