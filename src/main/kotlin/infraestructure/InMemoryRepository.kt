package infraestructure

import core.domain.User
import core.repository.UserRepository

class InMemoryRepository : UserRepository {
    private var storedUser: MutableList<User> = mutableListOf()

    override fun save(user: User) {
        storedUser.add(user)
    }

    override fun get(nickname: String): User {
        return storedUser.first {
            it.nickname == nickname
        }
    }
}