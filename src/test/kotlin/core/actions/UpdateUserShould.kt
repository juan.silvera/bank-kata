package core.actions

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import core.domain.User
import core.exceptions.UserNotFound
import core.repository.UserRepository
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class UpdateUserShould {
    private lateinit var updateUser: UpdateUser
    private lateinit var repository: UserRepository
    private var updateResult: Boolean = false

    @Before
    fun setup() {
        repository = mock()
    }

    @Test
    fun `update user name`() {
        givenAUpdateUser()
        givenAStoredUser()

        whenUpdatesUser()

        thenUpdatesUserName()
    }

    @Test(expected = UserNotFound::class)
    fun `show error if user is not found`() {
        givenAUpdateUser()
        whenever(repository.get(NICKNAME)).thenReturn(null)

        whenTriesToUpdateUser()
    }

    private fun givenAStoredUser() {
        whenever(repository.get(NICKNAME)).thenReturn(ORIGINAL_USER)
    }

    private fun givenAUpdateUser() {
        updateUser = UpdateUser(repository)
    }

    private fun whenUpdatesUser(){
        updateResult = updateUser.invoke(NEW_NAME, NICKNAME)
    }

    private fun whenTriesToUpdateUser(){
        updateUser.invoke(NEW_NAME, NICKNAME)
    }

    private fun thenUpdatesUserName() {
        assertEquals(true, updateResult)
    }

    companion object {
        private const val NAME = "Pepe Original"
        private const val NEW_NAME = "Pepito Updateado"
        private const val NICKNAME = "@peTwitter"
        private val ORIGINAL_USER = User(NAME, NICKNAME)
    }
}