package core.actions

import com.nhaarman.mockitokotlin2.*
import core.domain.User
import core.exceptions.UserAlreadyTakenError
import core.repository.UserRepository
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class RegisterUserShould {
    private lateinit var registerUser: RegisterUser
    private lateinit var repository: UserRepository
    private var userRegistered: Boolean = false

    @Before
    fun setup() {
        repository = mock()
    }

    @Test
    fun `create a new user`() {
        givenARegisterUser()
        givenANonExistentUser()

        whenRegistersUser(NAME, NICKNAME)

        thenRegisters()
    }

    @Test(expected = UserAlreadyTakenError::class)
    fun `not save user if nickname is taken`() {
        givenARegisterUser()
        givenASavedUser()

        whenRegistersUser(NAME, NICKNAME)
    }

    @Test
    fun `save user to repository`() {
        givenARegisterUser()
        givenANonExistentUser()

        whenRegistersUser(NAME, NICKNAME)

        thenSavesIntoRepository()
    }

    private fun givenASavedUser() {
        whenever(repository.get(NICKNAME)).thenReturn(USER)
    }

    private fun givenARegisterUser() {
        registerUser = RegisterUser(repository)
    }

    private fun givenANonExistentUser() {
        whenever(repository.get(NICKNAME)).thenReturn(null)
    }

    private fun whenRegistersUser(name: String, nickname: String) {
        userRegistered = registerUser.invoke(name, nickname)
    }

    private fun thenRegisters() {
        Assert.assertEquals(true, userRegistered)
    }

    private fun thenSavesIntoRepository() {
        verify(repository).save(USER)
    }

    companion object {
        private const val NAME = "Pepito Twitter"
        private const val NICKNAME = "@peTwitter"
        private val USER = User(NAME, NICKNAME)
    }
}