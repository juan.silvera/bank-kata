package infraestructure

import core.domain.User
import org.junit.Assert.*

import org.junit.Test

class InMemoryRepositoryShould {
    private lateinit var inMemoryRepository: InMemoryRepository
    private lateinit var foundUser: User

    @Test
    fun `save user`() {
        givenAnInMemoryRepository()
        givenASaveUser(NEW_USER)

        whenFindsSavedUser(NICKNAME)

        thenSavesUser()
    }

    @Test
    fun `get the first user when saving two`() {
        givenAnInMemoryRepository()
        givenASaveUser(NEW_USER)
        givenASaveUser(SECOND_NEW_USER)

        whenFindsSavedUser(NICKNAME)

        thenSavesUser()
    }

    private fun givenASaveUser(user: User) {
        inMemoryRepository.save(user)
    }

    private fun givenAnInMemoryRepository() {
        inMemoryRepository = InMemoryRepository()
    }

    private fun whenFindsSavedUser(nickname: String) {
        foundUser = inMemoryRepository.get(nickname)
    }

    private fun thenSavesUser() {
        assertEquals(NEW_USER, foundUser)
    }

    companion object {
        private val NEW_USER = User("Pepito Twitter", "@peTwitter")
        private val SECOND_NEW_USER = User("Carlitos Sin Twitter", "@caSinTwitter")
        private const val NICKNAME = "@peTwitter"
    }
}